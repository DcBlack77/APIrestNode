'use strict'

const express = require('express')
const bodyParser = require('body-parser')
const a = express()
const api = require('./routers')

a.use(bodyParser.urlencoded({ extended: false}))
a.use(bodyParser.json())
a.use('/api', api)

module.exports = a
