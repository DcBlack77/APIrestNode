'use strict'


const Producto = require('../models/producto')


function getProductos (req, res) {
    Producto.find({}, (err,products)=>{
        if (err) {
            return res.status(500).send({mensaje: `Ha ocurrido un error interno de Servidor: ${err}`})
        }
        if (!products) {
            return res.status(404).send({mensaje: `El prodcuto solicitado, no existe`})
        }
        res.status(200).send({products})
    })
}

function getProducto (req, res) {
    let productoId = req.params.productoId

    Producto.findById(productoId, (err, producto)=>{
        if (err) {
            return res.status(500).send({mensaje: `Ha ocurrido un error interno de Servidor: ${err}`})
        }
        if (!producto) {
            return res.status(404).send({mensaje: `El prodcuto solicitado, no existe`})
        }

        res.status(200).send({producto})
    })
}

function saveProducto(req, res) {
    console.log('POST api/producto')
    console.log(req.body)

    let producto = new Producto()
    producto.nombre = req.body.nombre
    producto.precio = req.body.precio
    producto.foto = req.body.foto
    producto.categoria = req.body.categoria
    producto.descripcion = req.body.descripcion

    producto.save((err, productoStored) => {
        if (err) res.status(500).send({message: `Error salvando en la base de datos: ${err}`})

        res.status(200).send({producto: productoStored})
    })
}

function uploadProducto (req, res) {
    console.log('PUT /api/producto')
    let productoId = req.params.productoId
    let cuerpo = req.body

    Producto.findByIdAndUpdate(productoId, cuerpo, (err, productoActualizado) =>{
        if (err) res.status(500).send({mensaje: `Error Actualizando el producto ${err}`})

        res.status(200).send(`El producto: ${productoId} ha sido actualizado con normalidad`)
    })
}

function deleteProducto (req, res) {
    console.log('DELETE /api/producto')
    let productoId = req.params.productoId

    Producto.findById(productoId,(err, producto)=>{
        if (err) res.status(500).send({mensaje: `Error eliminando el producto ${err}`})

        producto.remove( err => {
            if (err) res.status(500).send({mensaje: `Error eliminando el producto ${err}`})
            res.status(200).send({mensaje: `El producto con el ID: ${productoId} ha sido eliminado`})
        })
    })
}

module.exports = {
    getProductos,
    getProducto,
    saveProducto,
    uploadProducto,
    deleteProducto
}
