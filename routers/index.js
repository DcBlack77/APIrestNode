'use strict'

const express = require('express')
const ProductoCntrl = require('../controllers/producto')
const UsuarioCntrl = require('../controllers/usuario')
const auth = require('../middleware/auth')
const api = express.Router()


api.get('/producto', ProductoCntrl.getProductos)
api.get('/producto/:productoId', ProductoCntrl.getProducto)
api.post('/producto', ProductoCntrl.saveProducto)
api.put('/producto/:productoId', ProductoCntrl.uploadProducto)
api.delete('/producto/:productoId', ProductoCntrl.deleteProducto)
api.post('/signup', UsuarioCntrl.signUp)
api.post('/signin', UsuarioCntrl.signIn)
api.get('/privado', auth, function(req, res) {
    res.status(200).send({mensaje: 'Tienes acceso mariquito'})
})

module.exports = api
