'use strict'


const servicios = require('../servicios')

function isAuth (req, res, next) {
    if (!req.headers.authorization) {
        return res.status(403).send({ mensaje: ` No tienes permisos para acceder a esta parte.`})
    }
    const token = req.headers.authorization.split(" ")[1]
    servicios.decodeToken(token).then(response=>{
        req.usuario = response
        next()
    }).catch(response => {
        res.status(response.status)
    })
    next()
}


module.exports = isAuth;
