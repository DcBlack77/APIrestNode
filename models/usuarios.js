'use strict'

const mongoose = require('mongoose')
const Schema = mongoose.Schema
//Instalar dependencia del bcrypt-nodejs con NPM, por favor y gracias pequeño saltamonte
const bcrypt = require('bcrypt-nodejs');
//Tambiien instala la libreria "Crypto" para el md5 del gravatar
const crypto = require('crypto')

const UsuariosSchema = new Schema({
    correo: {type:String, unique: true, lowercase: true},
    nombre: String,
    apellido: String,
    contraseña: {type:String, select:false},
    username: String,
    imagen: String,
    fechaRegistro: {type:Date, default: Date.now()}
})

UsuariosSchema.pre('save', function (next) {
    let user = this
    if (!user.isModified('contraseña')) return next()

    bcrypt.genSalt(10, (err, salt)=>{
        if (err) return next(err)

        bcrypt.hash(user.contraseña, salt, null, (err, hash)=>{
            if (err) return next(err)

            user.contraseña = hash
            next()
        })
    })
})

UsuariosSchema.methods.gravatar = function (){
    if (!this.correo) return `https://gravatar.com/avatar/?s=200&d=retro`

    const md5 = cryto.createHash('md5').update(this.correo).digest('hex')
    return `https://gravatar.com/avatar/${md5}?s=200&d=retro`
}

module.exports = mongoose.model('usuarios', UsuariosSchema)
