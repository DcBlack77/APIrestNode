'use strict'

const mongoose = require('mongoose')
const Schema = mongoose.Schema

const ProductoSchema = Schema({
    nombre: String,
    precio: Number,
    foto: String,
    categoria: {
        type: String,
        enum: [
            'Nacional',
            'Internacional',
            'Computadoras',
            'Telefonos',
            'Otros'
        ]},
    descripcion: String
})

module.exports = mongoose.model('producto', ProductoSchema)
